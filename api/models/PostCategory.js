var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

var categorySchema = Schema({
	name: {
		type: String,
		minlength: 3,
		maxlength: 100,
		required: true
	},
	description: {
		type: String,
		default: 'Category Description'
	}
}, {
	collection: 'post-categories'
});

//format info to response
categorySchema.methods.format = function() {
	return JSON.parse(JSON.stringify(this, ['_id', 'name', 'description', 'posts']));
};

//update
categorySchema.methods.update = function(data) {
	if (data.hasOwnProperty('name')) this.name = data.name;
	if (data.hasOwnProperty('description')) this.description = data.description;
	return true;
};

module.exports = mongoose.model('PostCategory', categorySchema);