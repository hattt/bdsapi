var q = require('q'),
	mongoose = require('mongoose'),
	Schema = mongoose.Schema;
mongoose.Promise = q.Promise;

var postSchema = Schema({
	title: {
		type: String,
		minlength: 3,
		maxlength: 100,
		require: true
	},
	category: {
		type: Schema.Types.ObjectId,
		ref: 'PostCategory'
	},
	content: {
		type: String,
		required: true
	},
	author: {
		type: Schema.Types.ObjectId,
		ref: 'User',
		required: true
	},
	createDate: {
		type: Date,
		default: Date.now
	},
	images: [String],
	comments: [{
		text: {
			type: String,
			minlength: 3,
			required: true
		},
		by: {
			type: Schema.Types.ObjectId,
			ref: 'User',
			required: true
		},
		at: {
			type: Date,
			default: Date.now
		},
		// replies: [{
		// 	text: {
		// 		type: String,
		// 		minlength: 3,
		// 		required: true
		// 	},
		// 	by: {
		// 		type: Schema.Types.ObjectId,
		// 		ref: 'User',
		// 		required: true
		// 	},
		// 	at: {
		// 		type: Date,
		// 		default: Date.now
		// 	}
		// }]
	}]
});

//format info to response
postSchema.methods.format = function() {
	return JSON.parse(JSON.stringify(this, ['_id', 'title', 'category', 'content',
		'author', 'createDate', 'images', 'comments'
	]));
};

//update
postSchema.methods.update = function(data) {
	if (data.hasOwnProperty('title')) this.title = data.title;
	if (data.hasOwnProperty('content')) this.content = data.content;
	if (data.hasOwnProperty('images')) this.images = data.images;
	return true;
};

//comment
postSchema.methods.add_comment = function(data) {
	var comment = {
		at: Date.now
	};
	if (data.hasOwnProperty('text')) comment.text = data.text;
	if (data.hasOwnProperty('by')) comment.by = comment.by;
	if (data.hasOwnProperty('at')) comment.at = new Date(data.at);

	this.comments.push(comment);
	return true;
};


module.exports = mongoose.model('Post', postSchema)