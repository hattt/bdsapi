var mongoose = require('mongoose'),
	bcrypt = require('bcrypt-nodejs');

var userSchema = mongoose.Schema({
	username: {
		type: String,
		match: /^[a-zA-Z0-9_-]{6,}$/,
		required: true,
		unique: true
	},
	password: {
		type: String,
		required: true
	},
	name: {
		type: String,
		default: 'NAME'
	},
	admin: {
		type: Boolean,
		default: false
	}
});

//Hash password before save
userSchema.pre('save', function(cb) {
	var user = this;

	//password not change -> return
	if (!user.isModified('password')) return cb();

	//password change -> hash
	bcrypt.genSalt(5, function(err, salt) {
		if (err) return cb(err);
		bcrypt.hash(user.password, salt, null, function(err, hash) {
			if (err) return cb(err);
			user.password = hash;
			cb();
		});
	});
});

//Verify password
userSchema.methods.verifyPassword = function(password, cb) {
	bcrypt.compare(password, this.password, function(err, rlt) {
		if (err) cb(err);
		cb(null, rlt);
	});
};

//format info to response
userSchema.methods.format = function() {
	return JSON.parse(JSON.stringify(this, ['_id', 'username', 'name', 'admin']));
};

//update
userSchema.methods.update = function(data) {
	if (data.hasOwnProperty('password')) {
		if (data.password.length < 6 || data.password.length > 50) return false;
		this.password = data.password;
	}
	if (data.hasOwnProperty('admin')) this.admin = data.admin;
	if (data.hasOwnProperty('name')) this.name = data.name;
	return true;
};

module.exports = mongoose.model('User', userSchema);