var r = require('express').Router(),
	midd = require('../middlewares'),
	controller = require('../controllers/postController');

r.route('/')
	.get(controller.list_all_posts)
	.post(midd.admin_only, controller.create_a_post);

r.route('/categories/:catId')
	.get(controller.list_all_posts_by_category);

r.route('/:postId')
	.get(controller.read_a_post)
	.put(midd.get_post_author, midd.admin_or_author_only, controller.update_a_post)
	.delete(midd.get_post_author, midd.admin_or_author_only, controller.delete_a_post);

// r.route('/:postId/comments')
// 	.get(controller.read_a_post)
// 	.put(midd.get_post_author, midd.admin_or_author_only, controller.update_a_post)
// 	.delete(midd.get_post_author, midd.admin_or_author_only, controller.delete_a_post);
module.exports = r;