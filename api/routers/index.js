var r = require('express').Router(),
	midd = require('../middlewares');

//authenticate & provide jwt
r.use('/authenticate', require('./authenticate'));

//check & decode jwt
r.use(midd.jwt_authenticate);

r.get('/', (req, res) => {
	res.send('Welcome to bdsAPI!');
});
r.use('/users', require('./users'));
r.use('/posts', require('./posts'));
r.use('/post-categories', require('./post-categories'));

r.use(midd.error_handle, midd['404_handle']);

module.exports = r;