var r = require('express').Router(),
	midd = require('../middlewares'),
	controller = require('../controllers/userController');

r.route('/')
	.get(midd.admin_only, controller.list_all_users)
	.post(midd.admin_only, controller.create_a_user);

r.route('/:userId')
	.get(midd.admin_or_user_only, controller.read_a_user)
	.put(midd.admin_or_user_only,
		midd.only_admin_can_update_admin, controller.update_a_user)
	.delete(midd.admin_only, controller.delete_a_user);

module.exports = r;