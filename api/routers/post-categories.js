var r = require('express').Router(),
	midd = require('../middlewares'),
	controller = require('../controllers/post-categoryController');

r.route('/')
	.get(controller.list_all_cats)
	.post(midd.admin_only, controller.create_a_cat);

r.route('/:catId')
	.get(controller.read_a_cat)
	.put(midd.admin_only, controller.update_a_cat)
	.delete(midd.admin_only, controller.delete_a_cat);

module.exports = r;