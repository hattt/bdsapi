var r = require('express').Router(),
	controller = require('../controllers/authenticateController');

r.post('/', controller.authenticate);

module.exports = r;