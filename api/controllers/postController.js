var mongoose = require('mongoose'),
	User = mongoose.model('User'),
	Post = mongoose.model('Post'),
	CONFIG = require('../../config');



exports.list_all_posts = (req, res, next) => {
	Post.find({}, function(err, posts) {
		if (err) return next(new Error('404'));
		posts = posts.map((post) => {
			return post.format();
		});
		res.json(posts);
	});
};
exports.list_all_posts_by_category = (req, res, next) => {
	Post.find({
		category: req.params.catId
	}, function(err, posts) {
		if (err) return next(new Error('404'));

		posts = posts.map((post) => {
			return post.format();
		});
		res.json(posts);
	});
};

exports.create_a_post = (req, res, next) => {
	var post = new Post(req.body);
	post.save((err, new_post) => {
		console.log(err);
		if (err) return next(new Error('400'));
		res.json(new_post.format());
	});
};

exports.read_a_post = (req, res, next) => {
	Post.findById(req.params.postId, (err, post) => {
		if (err) return next(new Error('404'));
		res.json(post.format());
	});
};

exports.update_a_post = (req, res, next) => {
	Post.findById(req.params.postId, (err, post) => {
		if (err) return next(new Error('404'));

		if (!post.update(req.body)) return next(new Error('400'));
		post.save((err, updated_post) => {
			if (err) return next(new Error('400'));

			res.json(updated_post.format());
		});
	});
};

exports.delete_a_post = (req, res, next) => {
	Post.findByIdAndRemove(req.params.userId, (err, post) => {
		if (err) return next(new Error('404'));
		res.json({
			success: true,
			msg: 'Delete post success!'
		});
	});
};

// exports.comment_a_post = (req, res, next) => {
// 	Post.findById(req.params.postId, (err, post) => {
// 		if (err) return next(new Error('404'));

// 		if (!post.add_comment(req.body)) return next(new Error('400'));
// 		post.save((err, updated_post) => {
// 			if (err) return next(new Error('400'));

// 			res.json(updated_post.format());
// 		});
// 	});
// };