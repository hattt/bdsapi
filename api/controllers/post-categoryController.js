var mongoose = require('mongoose'),
	PostCategory = mongoose.model('PostCategory'),
	CONFIG = require('../../config');



exports.list_all_cats = (req, res, next) => {
	PostCategory.find({}, function(err, cats) {
		if (err) return next(new Error('404'));
		cats = cats.map((cat) => {
			return cat.format();
		});
		res.json(cats);
	});
};

exports.create_a_cat = (req, res, next) => {
	var cat = new PostCategory(req.body);
	cat.save((err, new_cat) => {

		if (err) return next(new Error('400'));
		res.json(new_cat.format());
	});
};

exports.read_a_cat = (req, res, next) => {
	PostCategory.findById(req.params.catId, (err, cat) => {
		if (err) return next(new Error('404'));
		res.json(cat.format());
	});
};

exports.update_a_cat = (req, res, next) => {
	PostCategory.findById(req.params.catId, function(err, cat) {
		if (err) return next(new Error('404'));

		if (!cat.update(req.body)) return next(new Error('400'));

		cat.save((err, updated_cat) => {
			if (err) return next(new Error('400'));

			res.json(updated_cat.format());
		});
	});
};

exports.delete_a_cat = (req, res, next) => {
	PostCategory.findByIdAndRemove(req.params.catId, (err, cat) => {
		if (err) return next(new Error('404'));
		res.json({
			success: true,
			msg: 'Delete catory success!'
		});
	});
};