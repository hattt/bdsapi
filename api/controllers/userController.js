var mongoose = require('mongoose'),
	User = mongoose.model('User'),
	CONFIG = require('../../config');



exports.list_all_users = (req, res, next) => {
	User.find({}, function(err, users) {
		if (err) return next(new Error('404'));
		users = users.map((user) => {
			return user.format();
		});
		res.json(users);
	});
};

exports.create_a_user = (req, res, next) => {
	var user = new User(req.body);
	user.save((err, new_user) => {

		if (err) return next(new Error('400'));
		res.json(new_user.format());
	});
};

exports.read_a_user = (req, res, next) => {
	User.findById(req.params.userId, (err, user) => {
		if (err) return next(new Error('404'));
		res.json(user.format());
	});
};

exports.update_a_user = (req, res, next) => {
	User.findById(req.params.userId, function(err, user) {
		if (err) return next(new Error('404'));

		if (!user.update(req.body)) return next(new Error('400'));

		user.save((err, updated_user) => {
			if (err) return next(new Error('400'));

			res.json(updated_user.format());
		});
	});
};

exports.delete_a_user = (req, res, next) => {
	User.findByIdAndRemove(req.params.userId, (err, user) => {
		if (err) return next(new Error('404'));
		res.json({
			success: true,
			msg: 'Delete user success!'
		});
	});
};