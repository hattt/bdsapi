var mongoose = require('mongoose'),
	User = mongoose.model('User'),
	jwt = require('jsonwebtoken'),
	CONFIG = require('../../config');

exports.authenticate = (req, res) => {
	User.findOne({
		username: req.body.username
	}, (err, user) => {
		if (err) return res.status(401).json({
			success: false,
			msg: 'Authentication failed!'
		});

		if (!user) return res.status(401).json({
			success: false,
			msg: 'Authentication failed! User not found.'
		});

		user.verifyPassword(req.body.password, (err, isCorrect) => {
			if (err) return res.status(401).json({
				success: false,
				msg: 'Authentication failed!'
			});
			if (!isCorrect) return res.status(401).json({
				success: false,
				msg: 'Authentication failed! Wrong password.'
			});

			//create token
			var expiredIn = user.admin ?
				CONFIG.JWT.expiredIn_admin : CONFIG.JWT.expiredIn_not_admin;
			jwt.sign({
				userId: user._id,
				admin: user.admin
			}, CONFIG.JWT.secret, {
				issuer: CONFIG.JWT.issuer,
				expiresIn: expiredIn
			}, (err, token) => {
				if (err) return res.json({
					success: false,
					msg: 'Authentication failed!'
				});
				res.status(200).json({
					success: true,
					msg: 'Your token will be expired after \'' + expiredIn + '\' ! Enjoy.',
					token: token
				});
			})
		});

	});
};