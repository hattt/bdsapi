var CONFIG = require('../../config');
var User = require('../models/User');

module.exports = (req, res, next) => {
	if (!req.jwt_decoded.admin) delete req.body.admin;
	next();
};