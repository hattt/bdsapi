module.exports = (err, req, res, next) => {
	var code = parseInt(err.message);
	var info = {
		success: false,
		msg: 'Error happened!'
	};

	if (process.env.NODE_ENV == 'dev' || process.env.NODE_ENV == 'test') info.msg = err.stack;
	res.status(code).send(info);
};