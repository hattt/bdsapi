var mongoose = require('mongoose'),
	Post = mongoose.model('Post'),
	CONFIG = require('../../config');

module.exports = (req, res, next) => {
	Post.findById(req.params.postId, (err, post) => {
		if (err) return res.json({
			success: false,
			msg: CONFIG.ERRORMSG.default
		});
		req.author = {
			userId: post.author
		};
		next();
	});
};