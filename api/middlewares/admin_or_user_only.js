var CONFIG = require('../../config');

module.exports = (req, res, next) => {
	if (req.jwt_decoded.admin ||
		req.jwt_decoded.userId == req.params.userId) return next();

	return res.status(403).json({
		success: false,
		msg: CONFIG.JWT.msg.AdminOnly
	});
};