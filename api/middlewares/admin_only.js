var CONFIG = require('../../config');

module.exports = (req, res, next) => {
	if (!req.jwt_decoded.admin) return res.status(403).json({
		success: false,
		msg: CONFIG.JWT.msg.AdminOnly
	});
	next();
};