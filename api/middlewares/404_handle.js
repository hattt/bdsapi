module.exports = (req, res) => {
	var info = {
		success: false,
		msg: 'NOT FOUND'
	};
	res.status(404).send(info);
};