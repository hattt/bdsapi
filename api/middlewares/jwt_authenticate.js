var jwt = require('jsonwebtoken'),
	CONFIG = require('../../config');

// check jwt
// - fail: response status 401
// - success: save decoded jwt at req.jwt_decoded
module.exports = (req, res, next) => {
	var token = req.body.token || req.query.token ||
		req.headers['x-access-token'];
	debugger;
	if (token) {
		jwt.verify(token, CONFIG.JWT.secret, (err, decoded) => {
			debugger;
			if (err) return res.status(401).json({
				success: false,
				msg: err.name == 'TokenExpiredError' ?
					'Token expired!' : 'Failed to authenticate token!'
			});

			req.jwt_decoded = decoded;
			return next();

		});
	} else {
		return res.status(401).json({
			success: false,
			msg: 'No token provided!'
		});
	}

};