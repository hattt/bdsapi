module.exports = {
	jwt_authenticate: require('./jwt_authenticate'),
	admin_only: require('./admin_only'),
	admin_or_user_only: require('./admin_or_user_only'),
	admin_or_author_only: require('./admin_or_author_only'),
	get_post_author: require('./get_post_author'),
	error_handle: require('./error_handle'),
	'404_handle': require('./404_handle'),
	only_admin_can_update_admin: require('./only_admin_can_update_admin')
};