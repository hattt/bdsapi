Learning Source:
(23/08/2017)
- https://www.codementor.io/olatundegaruba/nodejs-restful-apis-in-10-minutes-q0sgsfhbd#comments-q0sgsfhbd
- http://mongoosejs.com/docs/
(24/08/2017)
- https://scotch.io/tutorials/authenticate-a-node-js-api-with-json-web-tokens (jwt authentication)
- http://scottksmith.com/blog/2014/05/29/beer-locker-building-a-restful-api-with-node-passport/ (UserSchema: pre('save'), verifyPassword use bcrypt)

(25/08/2017)
- https://scotch.io/tutorials/test-a-node-restful-api-with-mocha-and-chai
- mocha, chai, chai-http, expect

(26/08/2017)
- http://mongoosejs.com/docs/validation.html (mongoose validation)

(27/08/2017)
- https://github.com/kriskowal/q (chaining, fcall) (apply for mongoose + chai-http)
- http://mongoosejs.com/docs/promises.html
http://chaijs.com/plugins/chai-http/
- mocha skip: description.skip(...)