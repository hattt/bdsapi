var express = require('express'),
	mongoose = require('mongoose'),
	bodyParser = require('body-parser'),
	logger = require('morgan');
var CONFIG = require('./config');
var app = express();

//MONGODB
//1 - Load created models
var User = require('./api/models/User'),
	Post = require('./api/models/Post'),
	PostCategory = require('./api/models/PostCategory');
//2 - Connect your database by adding a url to the mongoose instance connection
var dbConnection = mongoose.connect(CONFIG.DB.uri).connection;
dbConnection.on('error', console.log);
dbConnection.once('open', () => {
	if (process.env.NODE_ENV != 'test') console.log('MONGO: connect success');
});

//CONFIG SERVER
app.set('port', process.env.PORT || 3000);

//MIDDLEWARES
app.use(bodyParser.urlencoded({
	extended: true
}));
app.use(bodyParser.json());

app.use(logger('dev'));
//ROUTES
app.get('/', (req, res) => {
	res.send('Welcome to bdsAPI!');
});
app.use('/api', require('./api/routers'));

//START SERVER
app.listen(app.get('port'), () => {
	if (process.env.NODE_ENV != 'test')
		console.log('API Server is listening at port ' + app.get('port'));
});

module.exports = app;