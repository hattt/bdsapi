process.env.NODE_ENV = 'test';
var User = require('../api/models/User'),
	PostCategory = require('../api/models/PostCategory'),
	Post = require('../api/models/Post');

var q = require('q'),
	mongoose = require('mongoose');
var chai = require('chai'),
	chaiHttp = require('chai-http'),
	server = require('../server'),
	expect = chai.expect;

//apply q promise for mongoose & chai
mongoose.Promise = q.Promise;
global.Promise = q.Promise;

chai.use(chaiHttp);

module.exports = {
	info_user: {
		username: 'user123',
		password: '123456',
		admin: false
	},

	info_admin: {
		username: 'admin123',
		password: '123456',
		admin: true
	},
	info_other: {
		username: 'other123',
		password: '123456',
		admin: false
	},
	info_cat: {
		name: 'Bất động sản'
	},
	info_post: {
		title: 'Đất quận 9',
		content: '<p><strong>Hello </strong><em>Hello</em></p>',
		images: ['https://vcdn.tikicdn.com/cache/w1200/media/catalog/product/3/6/3640575138370_s_01.d20170722.t115028.304577.jpg']
	},
	id_user: '',
	id_admin: '',
	id_other: '',
	id_cat: '',
	id_post: '',

	exec: function() {
		var d = q.defer();

		var user = new User(this.info_user);
		var admin = new User(this.info_admin);
		var other = new User(this.info_other);
		var cat = new PostCategory(this.info_cat);


		var removeData = q.all([User.remove({}), Post.remove({}), PostCategory.remove({})]),
			createData = q.all([user.save(),
				admin.save(), other.save(), cat.save()
			]);

		removeData
			.then(() => {
				return createData;
			})
			.spread((user, admin, other, cat) => {
				this.id_user = '' + user._id;
				this.id_admin = '' + admin._id;
				this.id_other = '' + other._id;
				this.id_cat = '' + cat._id;

				var post = new Post(this.info_post);
				post.author = this.id_admin;
				post.category = this.id_cat;
				return post.save();
			})
			.then((post) => {
				this.id_post = '' + post._id;
				d.resolve();
			})
			.catch((err) => {
				d.reject(err);
			});
		return d.promise;
	}
}