process.env.NODE_ENV = 'test';
var User = require('../api/models/User');

var chai = require('chai'),
	chaiHttp = require('chai-http'),
	server = require('../server'),
	expect = chai.expect;

chai.use(chaiHttp);

describe.skip('*** ROUTE /api/authenticate', () => {
	var info = {
		username: 'user123',
		password: '123456',
		name: 'USER 1'
	};

	before((done) => {
		var user = new User({
			username: info.username,
			password: info.password,
			name: info.name
		});
		User.remove({}, (err) => {
			if (err) done(err);
			user.save(done);
		});
	});

	//POST
	describe('#1 - POST', () => {
		it('+ login info: correct -> receive jwt', (done) => {
			chai.request(server)
				.post('/api/authenticate')
				.send(info)
				.end((err, res) => {
					expect(res).to.have.status(200);
					expect(res.body.success).to.be.true;
					expect(res.body).to.have.property('token')
					done();
				});

		});
		it('- login info: username exist, wrong password -> 401', (done) => {
			chai.request(server)
				.post('/api/authenticate')
				.send({
					username: info.username,
					password: info.password + 'xxx'
				})
				.end((err, res) => {
					expect(res).to.have.status(401);
					expect(res.body.success).to.be.false;
					done();
				});
		});
		it('- login info: username not exist -> 401', (done) => {
			chai.request(server)
				.post('/api/authenticate')
				.send({
					username: info.username + 'xxx',
					password: 'xxx'
				})
				.end((err, res) => {
					expect(res).to.have.status(401);
					expect(res.body.success).to.be.false;
					done();
				});
		});
	})

});