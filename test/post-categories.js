process.env.NODE_ENV = 'test';

var mongoose = require('mongoose'),
	q = require('q'),
	chai = require('chai'),
	chaiHttp = require('chai-http'),
	server = require('../server'),
	expect = chai.expect;

var PostCategory = require('../api/models/PostCategory'),
	User = require('../api/models/User'),
	init = require('./init');

//apply q promise for mongoose & chai
mongoose.Promise = q.Promise;
global.Promise = q.Promise;

chai.use(chaiHttp);


describe.skip('*** ROUTE /api/post-categories', () => {

	var jwt_user, jwt_admin;

	//init data + get jwt
	beforeEach((done) => {

		init.exec()
			.then(() => {
				return q.all([chai.request(server)
					.post('/api/authenticate')
					.send(init.info_user),
					chai.request(server)
					.post('/api/authenticate')
					.send(init.info_admin)
				]);
			})
			.spread((res_user, res_admin) => {
				jwt_user = res_user.body.token;
				jwt_admin = res_admin.body.token;
				done();
			})
			.catch(done);
	});

	//POST & PUT data
	var info_create_valid = {
			name: 'DEMO NAME'
		},
		info_create_invalid = {};

	var info_update_valid = {
			name: 'UPDATED',
			description: 'UPDATED'
		},
		info_update_invalid = {
			name: ''
		};




	describe('*** ROUTE /', () => {
		// GET
		describe('#1 - GET', () => {
			it('+ admin -> get all cats success: 200', (done) => {
				chai.request(server)
					.get('/api/post-categories')
					.set('x-access-token', jwt_admin)
					.end((err, res) => {
						expect(res).to.have.status(200);
						expect(res.body).to.be.a('array');
						expect(res.body.length).to.eql(1);
						done();
					});
			});
			it('- user -> get all cats fail 403', (done) => {
				chai.request(server)
					.get('/api/post-categories')
					.set('x-access-token', jwt_user)
					.end((err, res) => {
						expect(res).to.have.status(403);
						expect(res.body.success).to.be.false;
						done();
					});
			});
		});

		// POST
		describe('#1 - POST', () => {
			it('+ admin @ create cat success: 200', (done) => {
				chai.request(server)
					.post('/api/post-categories')
					.set('x-access-token', jwt_admin)
					.send(info_create_valid)
					.end((err, res) => {
						expect(res).to.have.status(200);
						expect(res.body.name).to.equal(info_create_valid.name);
						done();
					});
			});
			it('- admin @ create cat fail (with invalid info) : 400', (done) => {
				chai.request(server)
					.post('/api/post-categories')
					.set('x-access-token', jwt_admin)
					.send(info_create_invalid)
					.end((err, res) => {
						expect(res).to.have.status(400);
						done();
					});
			});
			it('- user @ create cat fail: 403', (done) => {
				chai.request(server)
					.post('/api/post-categories')
					.set('x-access-token', jwt_user)
					.send(info_create_valid)
					.end((err, res) => {
						expect(res).to.have.status(403);
						done();
					});
			});
		});
	});

	describe('*** ROUTE /:catId', () => {

		// GET
		describe('#1 - GET', () => {
			it('+ user @ get cat success : 200', (done) => {
				chai.request(server)
					.get('/api/post-categories/' + init.id_cat)
					.set('x-access-token', jwt_user)
					.end((err, res) => {
						expect(res).to.have.status(200);
						expect(res.body).to.have.property('_id');
						expect(res.body._id).to.equal(init.id_cat);
						done();
					});
			});
			it('+ admin @ get cat success : 200', (done) => {
				chai.request(server)
					.get('/api/post-categories/' + init.id_cat)
					.set('x-access-token', jwt_admin)
					.end((err, res) => {
						expect(res).to.have.status(200);
						expect(res.body).to.have.property('_id');
						expect(res.body._id).to.equal(init.id_cat);
						done();
					});
			});
		});

		// PUT
		describe('#2 - PUT', () => {

			it('+ user @ update cat fail : 403', (done) => {
				chai.request(server)
					.put('/api/post-categories/' + init.id_cat)
					.set('x-access-token', jwt_user)
					.send(info_update_valid)
					.end((err, res) => {
						expect(res).to.have.status(403);
						done();
					});
			});

			it('+ admin @ update cat success : 200', (done) => {
				chai.request(server)
					.put('/api/post-categories/' + init.id_cat)
					.set('x-access-token', jwt_admin)
					.send(info_update_valid)
					.end((err, res) => {
						expect(res).to.have.status(200);
						expect(res.body.name).to.equal(info_update_valid.name);
						expect(res.body.description).to.equal(info_update_valid.description);
						done();
					});
			});
			it('+ admin @ update cat fail (with invalid info) : 400', (done) => {
				chai.request(server)
					.put('/api/post-categories/' + init.id_cat)
					.set('x-access-token', jwt_admin)
					.send(info_update_invalid)
					.end((err, res) => {
						expect(res).to.have.status(400);
						done();
					});
			});


		});

		// DELETE
		describe('#3 - DELETE', () => {
			it('- user @ delete cat fail: 403', (done) => {
				chai.request(server)
					.delete('/api/post-categories/' + init.id_cat)
					.set('x-access-token', jwt_user)
					.end((err, res) => {
						expect(res).to.have.status(403);
						done();
					});
			});
			it('+ admin @ delete cat success: 200', (done) => {
				chai.request(server)
					.delete('/api/post-categories/' + init.id_cat)
					.set('x-access-token', jwt_admin)
					.end((err, res) => {
						expect(res).to.have.status(200);
						expect(res.body.success).to.be.true;
						done();
					});
			});
		});


	});

});