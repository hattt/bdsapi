process.env.NODE_ENV = 'test';

var mongoose = require('mongoose'),
	q = require('q'),
	chai = require('chai'),
	chaiHttp = require('chai-http'),
	server = require('../server'),
	expect = chai.expect;

var PostCategory = require('../api/models/PostCategory'),
	User = require('../api/models/User'),
	Post = require('../api/models/Post'),
	init = require('./init');

//apply q promise for mongoose & chai
mongoose.Promise = q.Promise;
global.Promise = q.Promise;

chai.use(chaiHttp);


describe('*** ROUTE /api/posts', () => {

	var jwt_user, jwt_admin;

	//init data + get jwt
	beforeEach((done) => {

		init.exec()
			.then((cat) => {
				info_create_valid.author = init.id_admin;
				info_update_valid.author = init.id_admin;
				info_create_invalid.author = init.id_admin;
				info_update_invalid.author = init.id_admin;
				return q.all([chai.request(server)
					.post('/api/authenticate')
					.send(init.info_user),
					chai.request(server)
					.post('/api/authenticate')
					.send(init.info_admin)
				]);
			})
			.spread((res_user, res_admin) => {
				jwt_user = res_user.body.token;
				jwt_admin = res_admin.body.token;
				done();
			})
			.catch(done);
	});

	//POST & PUT data (added author after init)
	var info_create_valid = {
			title: 'DEMO',
			content: '<p><strong>Hello </strong><em>Hello</em></p>',
			images: ['https://vcdn.tikicdn.com/cache/w1200/media/catalog/product/3/6/3640575138370_s_01.d20170722.t115028.304577.jpg']
		},
		info_create_invalid = {
			title: '',
			content: '<p><strong>Hello </strong><em>Hello</em></p>',
			images: ['https://vcdn.tikicdn.com/cache/w1200/media/catalog/product/3/6/3640575138370_s_01.d20170722.t115028.304577.jpg']
		};

	var info_update_valid = {
			title: 'UPDATED',
			content: 'UPDATED',
			images: ['UPDATED', 'UPDATED']
		},
		info_update_invalid = {
			title: '',
			content: 'UPDATED',
			images: ['UPDATED', 'UPDATED']
		};




	describe('*** ROUTE /', () => {
		// GET
		describe('#1 - GET', () => {
			it('+ admin -> get all posts success: 200', (done) => {
				chai.request(server)
					.get('/api/posts')
					.set('x-access-token', jwt_admin)
					.end((err, res) => {
						expect(res).to.have.status(200);
						expect(res.body).to.be.a('array');
						expect(res.body.length).to.eql(1);
						done();
					});
			});
			it('- user -> get all posts success: 200', (done) => {
				chai.request(server)
					.get('/api/posts')
					.set('x-access-token', jwt_user)
					.end((err, res) => {
						expect(res).to.have.status(200);
						expect(res.body).to.be.a('array');
						expect(res.body.length).to.eql(1);
						done();
					});
			});
		});

		// POST
		describe('#1 - POST', () => {
			it('+ admin @ create post success: 200', (done) => {
				chai.request(server)
					.post('/api/posts')
					.set('x-access-token', jwt_admin)
					.send(info_create_valid)
					.end((err, res) => {
						// console.log(res.body);
						expect(res).to.have.status(200);
						expect(res.body.title).to.eql(info_create_valid.title);
						expect(res.body.content).to.eql(info_create_valid.content);
						expect(res.body.images).to.eql(info_create_valid.images);
						done();
					});
			});
			it('- admin @ create post fail (with invalid info) : 400', (done) => {
				chai.request(server)
					.post('/api/posts')
					.set('x-access-token', jwt_admin)
					.send(info_create_invalid)
					.end((err, res) => {
						expect(res).to.have.status(400);
						done();
					});
			});
			it('- user @ create post fail: 403', (done) => {
				chai.request(server)
					.post('/api/posts')
					.set('x-access-token', jwt_user)
					.send(info_create_valid)
					.end((err, res) => {
						expect(res).to.have.status(403);
						done();
					});
			});
		});
	});
	describe('*** ROUTE /', () => {
		// GET
		describe('#1 - GET', () => {
			it('+ admin -> get all posts success: 200', (done) => {
				chai.request(server)
					.get('/api/posts/categories/' + init.id_cat)
					.set('x-access-token', jwt_admin)
					.end((err, res) => {
						expect(res).to.have.status(200);
						expect(res.body).to.be.a('array');
						expect(res.body.length).to.eql(1);
						done();
					});
			});
			it('- user -> get all posts success: 200', (done) => {
				chai.request(server)
					.get('/api/posts/categories/' + init.id_cat)
					.set('x-access-token', jwt_user)
					.end((err, res) => {
						expect(res).to.have.status(200);
						expect(res.body).to.be.a('array');
						expect(res.body.length).to.eql(1);
						done();
					});
			});
		});
	});

	describe('*** ROUTE /:catId', () => {

		// GET
		describe('#1 - GET', () => {
			it('+ user @ get post success : 200', (done) => {
				chai.request(server)
					.get('/api/posts/' + init.id_post)
					.set('x-access-token', jwt_user)
					.end((err, res) => {
						expect(res).to.have.status(200);
						expect(res.body).to.have.property('_id');
						expect(res.body._id).to.eql(init.id_post);
						done();
					});
			});
			it('+ admin @ get post success : 200', (done) => {
				chai.request(server)
					.get('/api/posts/' + init.id_post)
					.set('x-access-token', jwt_admin)
					.end((err, res) => {
						expect(res).to.have.status(200);
						expect(res.body).to.have.property('_id');
						expect(res.body._id).to.eql(init.id_post);
						done();
					});
			});
		});

		// PUT
		describe('#2 - PUT', () => {

			it('+ user (not author) @ update post fail : 403', (done) => {
				chai.request(server)
					.put('/api/posts/' + init.id_post)
					.set('x-access-token', jwt_user)
					.send(info_update_valid)
					.end((err, res) => {
						expect(res).to.have.status(403);
						done();
					});
			});

			it('+ admin (is author) @ update post success : 200', (done) => {
				chai.request(server)
					.put('/api/posts/' + init.id_post)
					.set('x-access-token', jwt_admin)
					.send(info_update_valid)
					.end((err, res) => {
						expect(res).to.have.status(200);
						expect(res.body.title).to.eql(info_update_valid.title);
						expect(res.body.content).to.eql(info_update_valid.content);
						expect(res.body.images).to.eql(info_update_valid.images);
						done();
					});
			});
			it('+ admin @ update post fail (with invalid info) : 400', (done) => {
				chai.request(server)
					.put('/api/posts/' + init.id_post)
					.set('x-access-token', jwt_admin)
					.send(info_update_invalid)
					.end((err, res) => {
						expect(res).to.have.status(400);
						done();
					});
			});


		});

		// DELETE
		describe('#3 - DELETE', () => {
			it('- user @ delete cat fail: 403', (done) => {
				chai.request(server)
					.delete('/api/posts/' + init.id_post)
					.set('x-access-token', jwt_user)
					.end((err, res) => {
						expect(res).to.have.status(403);
						done();
					});
			});
			it('+ admin @ delete cat success: 200', (done) => {
				chai.request(server)
					.delete('/api/posts/' + init.id_post)
					.set('x-access-token', jwt_admin)
					.end((err, res) => {
						expect(res).to.have.status(200);
						expect(res.body.success).to.be.true;
						done();
					});
			});
		});


	});

});