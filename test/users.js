process.env.NODE_ENV = 'test';
var User = require('../api/models/User'),
	init = require('./init');

var q = require('q'),
	mongoose = require('mongoose');
var chai = require('chai'),
	chaiHttp = require('chai-http'),
	server = require('../server'),
	expect = chai.expect;

//apply q promise for mongoose & chai
mongoose.Promise = q.Promise;
global.Promise = q.Promise;

chai.use(chaiHttp);



describe.skip('*** ROUTE /api/users', () => {
	//3 user: user, admin, other
	var jwt_user, jwt_admin, jwt_other;

	//init data + get jwt
	beforeEach((done) => {

		init.exec()
			.then(() => {
				return q.all([chai.request(server)
					.post('/api/authenticate')
					.send(init.info_user),
					chai.request(server)
					.post('/api/authenticate')
					.send(init.info_admin),
					chai.request(server)
					.post('/api/authenticate')
					.send(init.info_other)
				]);
			})
			.spread((res_user, res_admin, res_other) => {
				jwt_user = res_user.body.token;
				jwt_admin = res_admin.body.token;
				jwt_other = res_other.body.token
				done();
			})
			.catch(done);
	});

	var info_create_valid = {
			username: 'demo123',
			password: '123456',
			admin: true
		},
		info_create_invalid = {
			username: 's',
			password: '123456',
			admin: false
		};

	var info_update_valid = {
			username: init.info_user.username,
			password: init.info_user.password + 'xxx',
			name: 'UPDATED',
			admin: true
		},
		info_update_invalid = {
			username: init.info_user.username,
			password: 'sss',
			admin: true
		}







	describe('*** ROUTE /', () => {
		// GET
		describe('#1 - GET', () => {
			it('+ admin -> get all users', (done) => {
				chai.request(server)
					.get('/api/users')
					.set('x-access-token', jwt_admin)
					.end((err, res) => {
						expect(res).to.have.status(200);
						expect(res.body).to.be.a('array');
						expect(res.body.length).to.eql(3);
						done();
					});
			});
			it('- user -> 403', (done) => {
				chai.request(server)
					.get('/api/users')
					.set('x-access-token', jwt_user)
					.end((err, res) => {
						expect(res).to.have.status(403);
						expect(res.body.success).to.be.false;
						done();
					});
			});
		});

		// POST
		describe('#1 - POST', () => {

			it('+ admin @ create account success: 200', (done) => {
				chai.request(server)
					.post('/api/users')
					.set('x-access-token', jwt_admin)
					.send(info_create_valid)
					.end((err, res) => {
						expect(res).to.have.status(200);
						expect(res.body.username).to.equal(info_create_valid.username);
						expect(res.body.admin).to.equal(info_create_valid.admin);
						done();
					});
			});
			it('- admin @ create account fail (with invalid info) : 400', (done) => {
				chai.request(server)
					.post('/api/users')
					.set('x-access-token', jwt_admin)
					.send(info_create_invalid)
					.end((err, res) => {
						expect(res).to.have.status(400);
						done();
					});
			});
			it('- user @ create account fail: 403', (done) => {
				chai.request(server)
					.post('/api/users')
					.set('x-access-token', jwt_user)
					.send(info_create_valid)
					.end((err, res) => {
						expect(res).to.have.status(403);
						done();
					});
			});
		});
	});

	describe('*** ROUTE /:userId', () => {

		// GET
		describe('#1 - GET', () => {
			it('+ user @ get user success : 200', (done) => {
				chai.request(server)
					.get('/api/users/' + init.id_user)
					.set('x-access-token', jwt_user)
					.end((err, res) => {
						expect(res).to.have.status(200);
						expect(res.body).to.have.property('_id');
						expect(res.body._id).to.equal(init.id_user);
						done();
					});
			});
			it('+ admin @ get user success : 200', (done) => {
				chai.request(server)
					.get('/api/users/' + init.id_user)
					.set('x-access-token', jwt_admin)
					.end((err, res) => {
						expect(res).to.have.status(200);
						expect(res.body).to.have.property('_id');
						expect(res.body._id).to.equal(init.id_user);
						done();
					});
			});
			it('- other @ get user fail : 403', (done) => {
				chai.request(server)
					.get('/api/users/' + init.id_user)
					.set('x-access-token', jwt_other)
					.end((err, res) => {
						expect(res).to.have.status(403);
						done();
					});
			});

		});

		// PUT
		describe('#2 - PUT', () => {

			it('+ user @ update user success (\'admin\' not change) : 200', (done) => {
				chai.request(server)
					.put('/api/users/' + init.id_user)
					.set('x-access-token', jwt_user)
					.send(info_update_valid)
					.end((err, res) => {
						expect(res).to.have.status(200);
						expect(res.body.username).to.equal(init.info_user.username);
						expect(res.body.admin).to.be.false;
						done();
					});
			});
			it('- user @ update user fail (with invalid data) : 200', (done) => {
				chai.request(server)
					.put('/api/users/' + init.id_user)
					.set('x-access-token', jwt_user)
					.send(info_update_invalid)
					.end((err, res) => {
						expect(res).to.have.status(400);
						done();
					});
			});

			it('- other @ update user fail : 403', (done) => {
				chai.request(server)
					.put('/api/users/' + init.id_user)
					.set('x-access-token', jwt_other)
					.send(info_update_valid)
					.end((err, res) => {
						expect(res).to.have.status(403);
						done();
					});
			});

			it('+ admin @ update user success (field \'admin\' change) : 200', (done) => {
				chai.request(server)
					.put('/api/users/' + init.id_user)
					.set('x-access-token', jwt_admin)
					.send(info_update_valid)
					.end((err, res) => {
						expect(res).to.have.status(200);
						expect(res.body.username).to.equal(init.info_user.username);
						expect(res.body.admin).to.be.true;
						done();
					});
			});


		});

		// DELETE
		describe('#3 - DELETE', () => {
			it('- user @ delete user fail: 403', (done) => {
				chai.request(server)
					.delete('/api/users/' + init.id_user)
					.set('x-access-token', jwt_user)
					.end((err, res) => {
						expect(res).to.have.status(403);
						done();
					});
			});

			it('- other @ delete user fail: 403', (done) => {
				chai.request(server)
					.delete('/api/users/' + init.id_user)
					.set('x-access-token', jwt_other)
					.end((err, res) => {
						expect(res).to.have.status(403);
						done();
					});
			});

			it('+ admin @ delete user success: 200', (done) => {
				chai.request(server)
					.delete('/api/users/' + init.id_user)
					.set('x-access-token', jwt_admin)
					.end((err, res) => {
						expect(res).to.have.status(200);
						expect(res.body.success).to.be.true;
						done();
					});
			});
		});


	});
});